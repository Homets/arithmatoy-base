#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  size_t length;
    char *c_lhs = strdup(lhs);
    char *c_rhs = strdup(rhs);
  if (strlen(lhs) > strlen(rhs)){
        length = strlen(lhs);
        reverse(c_lhs);
        reverse(c_rhs);
        c_rhs = fill_with_0(c_rhs,length);
  } else if (strlen(lhs) < strlen(rhs)){
        length = strlen(rhs);
        reverse(c_lhs);
        reverse(c_rhs);
        c_lhs = fill_with_0(c_lhs,length);
  }else {
        length = strlen(lhs);
        reverse(c_lhs);
        reverse(c_rhs);
  }

    int carry = 0;
    char *result = malloc(strlen(c_lhs) + 1);
    for (int i = 0; i < length;i++){
           char mid_ope[1]; //use it for logging
            if (VERBOSE){
                fprintf(stderr,"add: digit %c digit %c carry %d\n", c_lhs[i], c_rhs[i], carry);
            }
            //addition if if carry exist
            if (carry == 1){

               if (get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry < base){
                        mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry);
                        result[i] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry);
                        carry = 0; //reset carry because result < base
                       if (VERBOSE){
                            fprintf(stderr,"add: result: digit %c carry %d\n",to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry), carry);

                        }

                } else {

                       mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry - base);
                       result[i] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry - base);
                       if (VERBOSE){
                            fprintf(stderr,"add: result: digit %c carry %d\n", to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry - base), carry);

                        }


                }

            } else {
                if (get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) < base){
                       mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]));
                       result[i] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]));
                        if (VERBOSE){
                            fprintf(stderr,"add: result: digit %c carry %d\n",to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i])) , carry);

                        }

                } else {
                        result[i] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) - base);
                        mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) - base);
                        carry = 1; //put carry to 1 because result > base
                        if (VERBOSE){
                            fprintf(stderr,"add: result: digit %c carry %d\n", to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) - base), carry);

                        }
                }



        }
    }

        if (carry >= 1){ //if a carry stay need to add length
            char *res = realloc(result, strlen(result) + 1);
            res[strlen(c_lhs)] = '1';
            res[strlen(c_lhs) + 1] = '\0';
            if (VERBOSE){
                fprintf(stderr,"add: final carry %d\n", carry);
            }

            return drop_leading_zeros(reverse(res));
        } else {
        result[strlen(c_lhs)] = '\0';
        return drop_leading_zeros(reverse(result));
        }

}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }
    char *c_lhs = strdup(lhs);
    char *c_rhs = strdup(rhs);
    size_t length;
    //check if the lhs without 0 leading 0 is shorter or the first digit is lower than rhs
    const char *lhs_without_leading_0 = drop_leading_zeros(lhs);
    const char *rhs_without_leading_0 = drop_leading_zeros(rhs);
    if (strlen(lhs_without_leading_0) < strlen(rhs_without_leading_0)){
        return NULL;
    } else if (strlen(lhs) == strlen(rhs) && lhs_without_leading_0[0] < rhs_without_leading_0[0]){
        return NULL;
    }



  if (strlen(lhs) > strlen(rhs)){
        length = strlen(lhs);
        reverse(c_lhs);
        reverse(c_rhs);
        c_rhs = fill_with_0(c_rhs,length);
  } else if (strlen(lhs) < strlen(rhs)){
        length = strlen(rhs);
        reverse(c_lhs);
        reverse(c_rhs);
        c_lhs = fill_with_0(c_lhs,length);
  }else {
        length = strlen(lhs);
        reverse(c_lhs);
        reverse(c_rhs);
  
  } 
    int carry = 0; 
    char *result = malloc(strlen(c_lhs) + 1);
    for (int i = 0; i < length;i++){
           char mid_ope[1]; //use it for logging
            if (VERBOSE){
                fprintf(stderr,"add: digit %c digit %c carry %d\n", c_lhs[i], c_rhs[i], carry);
            }
            //addition if if carry exist
            if (carry == 1){
               
               if (get_digit_value(c_lhs[i])  > 0 && get_digit_value(c_lhs[i])  > get_digit_value(c_rhs[i]) ){
                        
                        mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]) - carry);
                        result[i] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]) - carry);
                        carry = 0; //reset carry because result < base
                       
                } else {
                       
                       mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]) - carry + base);
                       result[i] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]) - carry + base);
                        
                
                }
            
            } else {
                if (get_digit_value(c_lhs[i]) >= get_digit_value(c_rhs[i])){
                       mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]));
                       result[i] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]));
                        
                
                } else {
                       result[i] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]) + base);
                        mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) - get_digit_value(c_rhs[i]) + base);
                        carry = 1; //put carry to 1 because result > base
                }

            
            }
        if (VERBOSE){
        fprintf(stderr,"sub: result: digit %c carry %d\n", mid_ope[0], carry);

        }
    }   
        result[strlen(c_lhs)] = '\0';
        return drop_leading_zeros(reverse(result));
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
        fprintf(stderr, "mul: entering function\n");
    }
  
    if (rhs == "0"){
     return "0";
    }
    if (lhs == "0"){
     return "0";
    }
    if (lhs == "1"){
        return rhs;
    }
    if (rhs == "1"){
        return lhs;
    }
    size_t length;
    char *c_lhs = strdup(lhs);
    char *c_rhs = strdup(rhs);
    if (strlen(lhs) > strlen(rhs)){
        length = strlen(lhs);
        reverse(c_lhs);
        reverse(c_rhs);
    } else if (strlen(lhs) < strlen(rhs)){
        length = strlen(rhs);
        reverse(c_lhs);
        reverse(c_rhs);
    }else {
        length = strlen(lhs);
        reverse(c_lhs);
        reverse(c_rhs);
    }
    char *final_res = "0";
    for (int i = 0; i < strlen(c_rhs);i++){
        if (VERBOSE){
            fprintf(stderr,"mul: result: digit %c number %s\n", c_rhs[i], lhs);
        }
        final_res = arithmatoy_add(base, final_res,multiple_with_lhs(c_rhs[i], c_lhs,i, base));
    }
    if (VERBOSE){
        fprintf(stderr, "mul: result %s", final_res);
    }
    return final_res;
    
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}
char *fill_with_0(char *digit, size_t length){
    char *digit_fill = malloc(length);
    for (size_t i = 0; i < length;i++){
        if (i == length){
            digit_fill[i] = '\0';
        } else if (i < strlen(digit)){
           digit_fill[i] = digit[i];
        } else {
            digit_fill[i] = '0';
        }
    }
    return digit_fill;

}
char *multiple_with_lhs(char digit, char *lhs,int ite, unsigned int base){
    char *result = malloc(strlen(lhs) +  ite);
    int carry = 0;
    for (int i = 0; i < strlen(lhs) + ite;i++){
        if (i < ite){
            result[i] = '0';
        } else {
               if (carry >= 1){

               if (get_digit_value(digit) * get_digit_value(lhs[i - ite]) + carry < base){
                      // mid_ope[0] = to_digit(get_digit_value(digit) + get_digit_value(c_rhs[i]) + carry);
                       result[i] = to_digit(get_digit_value(digit) * get_digit_value(lhs[i - ite]) + carry);
                       carry = 0; //reset carry because result < base

                } else {
                       //mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) + carry - base);
                       result[i] = to_digit((get_digit_value(digit) * get_digit_value(lhs[i - ite]) + carry) % base);
                       carry = (get_digit_value(digit) * get_digit_value(lhs[i - ite]) + carry) / base;


                }

            } else {
                if (get_digit_value(digit) * get_digit_value(lhs[i - ite]) < base){

                       //mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]));
                       result[i] = to_digit(get_digit_value(digit) * get_digit_value(lhs[i - ite]) % base);


                } else {
                       // mid_ope[0] = to_digit(get_digit_value(c_lhs[i]) + get_digit_value(c_rhs[i]) - base);
                       result[i] = to_digit(get_digit_value(digit) * get_digit_value(lhs[i - ite]) % base);
                       carry = get_digit_value(digit) * get_digit_value(lhs[i - ite]) / base;

                }

            }
        }
        fprintf(stderr,"mul: digit %c digit %c carry %d\n", digit, lhs[i - ite] , carry);

    }
    if (carry >= 1){ //if a carry stay need to add length
            char *res = realloc(result, strlen(result) + 1);
            res[strlen(lhs) + ite] = to_digit(carry);
            res[strlen(lhs)+ ite + 1] = '\0';
            if (VERBOSE){
                fprintf(stderr, "add: final carry %d\n", carry);
            }
            return  reverse(res);
        } else {
            result[strlen(lhs) + ite] = '\0';
            return reverse(result);

    }
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
